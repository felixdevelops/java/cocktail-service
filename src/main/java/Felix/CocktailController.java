package Felix;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CocktailController {

    private CocktailService cocktailService;

    public CocktailController(CocktailService cocktailService) {
        this.cocktailService = cocktailService;
    }
   /* @GetMapping("/cocktail")
    public String getCocktails() {
    return cocktailService.Name();
    }*/

    @GetMapping("/cocktail")
    public List<String> postCocktails(@RequestParam(value = "ingredient", defaultValue = "ice") String ingredient) throws Exception {
        return cocktailService.getApiInformation(ingredient);
    }

    @GetMapping("/uniquechar")
    public ArrayList<String> uniqueChar(@RequestParam(value = "string", defaultValue = "Mojito") String stringValue) throws Exception {
        return cocktailService.getuniqueChar(stringValue);
    }

    @GetMapping("/cocktailingredient")
    public List<String> cocktailIngredient(@RequestParam(value = "cocktail", defaultValue = "Mojito") String cocktailName) throws Exception {
        return cocktailService.getIngredients(cocktailName);
    }

    @GetMapping("/sort")
    public int[] getsortedList() throws Exception {
        return cocktailService.sort();
    }

    @GetMapping("/ID")
    public String getID(@RequestParam(value = "id", defaultValue = "3") Integer id) {
        return cocktailService.getID(id);
    }
}
