package Felix;

import lombok.Getter;
import lombok.Setter;

public class CocktailDto {

    @Getter
    @Setter
    public String cocktailID;
    public String cocktailName;

}
