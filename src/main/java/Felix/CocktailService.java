package Felix;

import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

import com.google.gson.*;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import lombok.Getter;
import lombok.Setter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;


@Service
public class CocktailService {
    @Getter
    @Setter
    public JSONObject jsonObjectIngredient;
    public JSONObject jsonObjectCocktail;

    private boolean hashsetFilled = false;
    private HashMap<Integer, String> idSet = new HashMap<Integer, String>();

    public List<CocktailDto> listDto = new ArrayList<>();

    public List<String> getApiInformation(String ingredient) throws Exception {
        // Host url
        String host = "https://the-cocktail-db.p.rapidapi.com/filter.php";
        String charset = "UTF-8";
        // Headers for a request
        String x_rapidapi_host = "the-cocktail-db.p.rapidapi.com";
        String x_rapidapi_key = "PLACEHOLDER_KEY";//Type here your key
        // Params
        String i = ingredient;
        // Format query for preventing encoding problems
        String query = String.format("i=%s", URLEncoder.encode(i, charset));
        HttpResponse<JsonNode> response = Unirest.get(host + "?" + query)
                .header("x-rapidapi-host", x_rapidapi_host)
                .header("x-rapidapi-key", x_rapidapi_key)
                .asJson();
        //Prettifying
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(response.getBody().toString());
        String prettyJsonString = gson.toJson(je);

        jsonObjectIngredient = response.getBody().getObject();
        JSONObject jo = response.getBody().getObject();
        List<String> list = new ArrayList<String>();
        JSONArray array = jo.getJSONArray("drinks");
        for (int a = 0; a < array.length(); a++) {
            list.add(array.getJSONObject(a).getString("strDrink"));
            CocktailDto dto = new CocktailDto();
            dto.cocktailID = array.getJSONObject(a).getString("idDrink");
            dto.cocktailName = array.getJSONObject(a).getString("strDrink");
            listDto.add(dto);
        }
        System.out.println(prettyJsonString);
        return list;
    }

    public ArrayList<String> getuniqueChar(String stringValue) {
        var charArray = stringValue.toCharArray();
        ArrayList uniqueLetters = new ArrayList<>();
        for (int i = 0; i < charArray.length; i++) {
            if (!uniqueLetters.contains(String.valueOf(charArray[i]))) {
                uniqueLetters.add(String.valueOf(charArray[i]));
            }
        }
        return uniqueLetters;
    }

    public List<String> getIngredients(String cocktailName) throws Exception {
        String host = "https://the-cocktail-db.p.rapidapi.com/lookup.php";
        String charset = "UTF-8";
        String x_rapidapi_host = "the-cocktail-db.p.rapidapi.com";
        String x_rapidapi_key = "f1fa3a1ceamsh1b8a99a01d82365p1ee026jsn649589a4d6f5";//Type here your key
        String i = getCocktailId(cocktailName);
        String query = String.format("i=%s", URLEncoder.encode(i, charset));
        HttpResponse<JsonNode> responseCocktail = Unirest.get(host + "?" + query)
                .header("x-rapidapi-host", x_rapidapi_host)
                .header("x-rapidapi-key", x_rapidapi_key)
                .asJson();
        jsonObjectCocktail = responseCocktail.getBody().getObject();
        JSONArray array = jsonObjectCocktail.getJSONArray("drinks");
        ArrayList<String> listIngredients = new ArrayList();
        for (int a = 1; a < 15; a++) {
            if (array.getJSONObject(0).getString("strIngredient" + a) != "null") {
                listIngredients.add(array.getJSONObject(0).getString("strIngredient" + a));
            }
        }
        return listIngredients;
    }

    private String getCocktailId(String cocktailName) {
        List<CocktailDto> list = listDto
                .stream()
                .filter(s -> s.cocktailName.equals(cocktailName))
                .collect(Collectors.toList());
        return list.get(0).cocktailID;
    }

    public int[] sort() {
        int[] unsortedArray = new int[]{13, 78, 32, 6, 12, 3, 45, 67};
        for (int a = 1; a < unsortedArray.length; a++) {
            for (int i = 0; i < unsortedArray.length - a; i++) {
                if (unsortedArray[i] > unsortedArray[i + 1]) {
                    int tmp = unsortedArray[i];
                    unsortedArray[i] = unsortedArray[i + 1];
                    unsortedArray[i + 1] = tmp;
                }
            }
        }
        return unsortedArray;
    }

    public String getID(Integer id) {
        if (!hashsetFilled) {
            idSet.put(0, "null");
            idSet.put(1, "eins");
            idSet.put(2, "zwei");
            hashsetFilled = true;
        }
        if (idSet.containsKey(id)) {
            return idSet.get(id);
        } else {
            return "Zahl ist nicht vergeben";
        }
    }
}
